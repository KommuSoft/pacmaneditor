//
//  PacmanScenario.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2013 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using Cairo;
using System.Text;
using System.IO;

namespace PacmanEditor {

    public class PacmanScenario {

        private PacmanPiece[,] pieces;
        private PacmanPiece pacmanLoc;
        private int teleportId = 0x00;
        private bool teleport = false;

        public PacmanScenario (int m, int n) {
            this.pieces = new PacmanPiece[m, n];
            for (int i = 0x00; i < m; i++) {
                for (int j = 0x00; j < n; j++) {
                    this.pieces [i, j] = new PacmanPiece ();
                }
            }
        }

        public void Paint (Context ctx, PointD size) {
            int m = pieces.GetLength (0x00);
            int n = pieces.GetLength (0x01);
            double dxy = Math.Min (size.X / m, size.Y / n);
            double y0 = 0.5d * (size.Y - dxy * m);
            double x0 = 0.5d * (size.X - dxy * n);
            PointD sz = new PointD (dxy, dxy);
            for (int i = 0x00; i < m; i++) {
                for (int j = 0x00; j < n; j++) {
                    ctx.Save ();
                    ctx.Translate (x0 + j * dxy, y0 + i * dxy);
                    this.pieces [i, j].Paint (ctx, sz);
                    ctx.Restore ();
                }
            }

            

            for (int i = 0x00; i <= m; i++) {
                ctx.MoveTo (x0 + i * dxy, y0);
                ctx.RelLineTo (0.0d, size.Y - 2 * y0);
            }
            for (int i = 0x00; i <= n; i++) {
                ctx.MoveTo (x0, y0 + i * dxy);
                ctx.RelLineTo (size.X - 2 * x0, 0.0d);
            }
            ctx.SetDash (new double[] {0.1 * dxy}, 0.0d);
            ctx.Color = new Color (0.5d, 0.5d, 0.5d);
            ctx.LineWidth = 0.5d;
            ctx.Stroke ();
            ctx.Color = new Color (0.0d, 0.0d, 0.0d);
            ctx.LineWidth = 4.0d;
            ctx.SetDash (new double[] {}, 0.0d);
            for (int i = 0x00; i < m; i++) {
                edgeDetectX (ctx, dxy, x0, y0 + i * dxy, i, n);
            }
            for (int j = 0x00; j < n; j++) {
                edgeDetectY (ctx, dxy, x0 + j * dxy, y0, j, n);
            }
            ctx.Stroke ();
        }

        public void retrieveTunnelIndices (int tunnelid, out int i0, out int j0, out int i1, out int j1) {
            int m = pieces.GetLength (0x00);
            int n = pieces.GetLength (0x01);
            i0 = i1 = j0 = j1 = -0x01;
            for (int i = 0x00; i < m; i++) {
                for (int j = 0x00; j < n; j++) {
                    PacmanPiece p = this.pieces [i, j];
                    if (p != null && p.Grass != false && p.Teleport == tunnelid) {
                        i0 = i1;
                        j0 = j1;
                        i1 = i;
                        j1 = j;
                    }
                }
            }
        }

        public PacmanPiece getPiece (double x, double y, PointD size) {
            int m = pieces.GetLength (0x00);
            int n = pieces.GetLength (0x01);
            double dxy = Math.Min (size.X / m, size.Y / n);
            double y0 = 0.5d * (size.Y - dxy * m);
            double x0 = 0.5d * (size.X - dxy * n);
            int j = (int)Math.Floor ((x - x0) / dxy);
            int i = (int)Math.Floor ((y - y0) / dxy);
            return pieceOrNull (i, j);
        }

        private PacmanPiece pieceOrNull (int i, int j) {
            int m = pieces.GetLength (0x00);
            int n = pieces.GetLength (0x01);
            if (i >= 0x00 && j >= 0x00 && i < m && j < n) {
                return this.pieces [i, j];
            } else {
                return null;
            }
        }
        public PacmanPiece getGridCoordinate (double x, double y, PointD size, out int i, out int j) {
            int m = pieces.GetLength (0x00);
            int n = pieces.GetLength (0x01);
            double dxy = Math.Min (size.X / m, size.Y / n);
            double y0 = 0.5d * (size.Y - dxy * m - 0.5d * dxy);
            double x0 = 0.5d * (size.X - dxy * n - 0.5d * dxy);
            j = (int)Math.Floor ((x - x0) / dxy);
            i = (int)Math.Floor ((y - y0) / dxy);
            return pieceOrNull (i, j);
        }

        private void edgeDetectX (Context ctx, double dxy, double x0, double y0, int i, int n) {
            bool oldgrass = false;
            bool grass = false;
            for (int j = 0x00; j < n; j++) {
                grass = this.pieces [i, j].Grass;
                if (grass != oldgrass) {
                    ctx.MoveTo (x0 + j * dxy, y0);
                    ctx.RelLineTo (0, dxy);
                }
                oldgrass = grass;
            }
            if (grass) {
                ctx.MoveTo (x0 + n * dxy, y0);
                ctx.RelLineTo (0, dxy);
            }
        }

        private void edgeDetectY (Context ctx, double dxy, double x0, double y0, int j, int m) {
            bool oldgrass = false;
            bool grass = false;
            for (int i = 0x00; i < m; i++) {
                grass = this.pieces [i, j].Grass;
                if (grass != oldgrass) {
                    ctx.MoveTo (x0, y0 + i * dxy);
                    ctx.RelLineTo (dxy, 0.0d);
                }
                oldgrass = grass;
            }
            if (grass) {
                ctx.MoveTo (x0, y0 + m * dxy);
                ctx.RelLineTo (dxy, 0.0d);
            }
        }

        public void GreenPoint (double x, double y, PointD size) {
            PacmanPiece piece = this.getPiece (x, y, size);
            if (piece != null) {
                piece.Green ();
            }
        }

        public void ClearGreenPoint (double x, double y, PointD size) {
            PacmanPiece piece = this.getPiece (x, y, size);
            if (piece != null) {
                int tele = piece.Teleport;
                if (tele >= 0x00) {
                    foreach (PacmanPiece other in this.pieces) {
                        if (other != null && other.Teleport == tele) {
                            other.clearTeleport ();
                        }
                    }
                }
                piece.ClearGreen ();
            }
        }

        public void setMenPoint (double x, double y, PointD size) {
            if (this.pacmanLoc != null) {
                this.pacmanLoc.Pacman = false;
            }
            this.pacmanLoc = this.getPiece (x, y, size);
            if (this.pacmanLoc != null) {
                this.pacmanLoc.Pacman = true;
            }
        }

        public void WallPoints (double x0, double y0, double x1, double y1, PointD size, bool wall) {
            int angle = (int)Math.Floor ((Math.Atan2 (y1 - y0, x1 - x0) + 0.25d * Math.PI) / (0.5d * Math.PI)) & 0x03;
            int i, j;
            this.getGridCoordinate (Math.Min (x0, x1), Math.Min (y0, y1), size, out i, out j);
            angle &= 0x01;
            PacmanPiece p = this.pieceOrNull (i, j);
            if (p != null) {
                if (angle == 0x00) {
                    p.TopWall = wall;
                } else {
                    p.LeftWall = wall;
                }
            }
        }

        public void TeleportPoint (double x, double y, PointD size) {
            PacmanPiece piece = this.getPiece (x, y, size);
            if (piece != null && piece.Grass) {
                piece.Teleport = this.teleportId;
                if (this.teleport) {
                    this.teleportId++;
                }
                this.teleport = !this.teleport;
            }
        }

        public void Save (TextWriter tw) {
            int m = pieces.GetLength (0x00);
            int n = pieces.GetLength (0x01);
            string aco = "{";
            string acc = "}";
            tw.WriteLine("structure made : V{");
            tw.WriteLine ("yCoord = {0}0..{2}{1}", aco, acc, n-1);
            tw.WriteLine ("xCoord = {0}0..{2}{1}", aco, acc, m-1);
            tw.Write ("nopos = {0}", aco);
            int ii = 0x00, ij = 0x00;
            StringBuilder sb = new StringBuilder ("wall = {");
            for (int i = 0x00; i < m; i++) {
                for (int j = 0x00; j < n; j++) {
                    PacmanPiece p = this.pieces [i, j];
                    if (p == null || p.Grass == false) {
                        tw.Write ("{0},{1};", i, j);
                    } else {
                        if (p.TopWall) {
                            sb.AppendFormat ("{0},{1},d;", i, j);
                        }
                        if (p.LeftWall) {
                            sb.AppendFormat ("{0},{1},r;", i, j);
                        }
                        if (p.Pacman) {
                            ii = i;
                            ij = j;
                        }
                    }
                }
            }
            tw.WriteLine ("}");
            tw.WriteLine (sb.ToString () + "}");
            tw.WriteLine ("Time={0}0..{2}{1}", aco, acc, m * n);
            tw.WriteLine ("maxscore={0}0..{2}{1}", aco, acc, m * n);
            tw.Write ("tunnel={");
            for (int t = 0x00; t < this.teleportId; t++) {
                int i0, i1, j0, j1;
                this.retrieveTunnelIndices (t, out i0, out j0, out i1, out j1);
                if (i0 != -0x01) {
                    tw.Write ("{1},{0},{3},{2};", i0, j0, i1, j1);
                }
            }
            tw.WriteLine ("}");
            tw.WriteLine ("initplayer={0}{2},{3}{1}", aco, acc, ii, ij);
            tw.WriteLine("l=l");
            tw.WriteLine("r=r");
            tw.WriteLine("d=d");
            tw.WriteLine("u=u");
            tw.WriteLine("}");
        }

    }
}

