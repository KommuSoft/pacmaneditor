//
//  TopWindow.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2013 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Reflection;
using Gtk;
using Cairo;
using System.IO;

namespace PacmanEditor {
    public partial class TopWindow : Gtk.Window {
        public TopWindow () : 
                base(Gtk.WindowType.Toplevel) {
            this.Build ();
        }

        public static int Main (string[] args) {
            Application.Init ();
            /*Gdk.Threads.Init();
            Gdk.Threads.Enter();*/
            using (TopWindow mw = new TopWindow()) {
                Application.Run ();
            }
            return 0;
        }

        protected void setGrass (object sender, EventArgs e) {
            this.sketchpad1.Tool = SketchpadTool.Grass;
        }
        protected void setWall (object sender, EventArgs e) {
            this.sketchpad1.Tool = SketchpadTool.Wall;
        }
        protected void setTeleport (object sender, EventArgs e) {
            this.sketchpad1.Tool = SketchpadTool.Teleport;
        }
        protected void setClearGrass (object sender, EventArgs e) {
            this.sketchpad1.Tool = SketchpadTool.ClearGrass;
        }
        protected void setClearWall (object sender, EventArgs e) {
            this.sketchpad1.Tool = SketchpadTool.ClearWall;
        }
        protected void setMen (object sender, EventArgs e) {
            this.sketchpad1.Tool = SketchpadTool.Men;
        }
        protected void saveAs (object sender, EventArgs e) {
            FileChooserDialog fcd = new FileChooserDialog ("Save as...", this, FileChooserAction.Save, "Cancel", ResponseType.Cancel, "Save", ResponseType.Accept);
            fcd.CurrentName = "file.idp";
            FileFilter ff = new FileFilter ();
            ff.Name = "IDP file (*.idp)";
            ff.AddPattern ("*.idp");
            fcd.AddFilter (ff);
            fcd.DefaultResponse = ResponseType.Accept;
            ResponseType rt = (ResponseType)fcd.Run ();
            if (rt == ResponseType.Accept) {
                FileStream fs = File.Open (fcd.Filename, FileMode.Create);
                TextWriter tw = new StreamWriter (fs);
                this.sketchpad1.Save (tw);
                tw.Close ();
                fs.Close ();
            }
            fcd.Destroy ();
        }

    }
}

