//
//  Sketchpad.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2013 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using Cairo;
using System.IO;

namespace PacmanEditor {

    [ToolboxItem(true)]
    public class SketchPad : CairoWidget {

        public const double Margin = 0x08;
        private PacmanScenario scen = new PacmanScenario (10, 10);
        private SketchpadTool tool = SketchpadTool.Grass;
        private PointD size;
        private PointD oldPoint;

        public SketchpadTool Tool {
            get {
                return tool;
            }
            set {
                tool = value;
            }
        }
        public SketchPad () {
            this.AddEvents ((int)(Gdk.EventMask.PointerMotionMask | Gdk.EventMask.ButtonPressMask | Gdk.EventMask.ButtonReleaseMask));
        }

        protected override bool OnMotionNotifyEvent (Gdk.EventMotion evnt) {
            /*if (this.rootpiece != null) {
                int index;
                IPuzzlePiece ipp = this.rootpiece.GetPuzzleGap (this.subcontext, new PointD (evnt.X - 5.0d, evnt.Y - 5.0d), out index);
                if (ipp != null) {
                    this.GdkWindow.Cursor = new Gdk.Cursor (Gdk.CursorType.CenterPtr);
                } else {
                    this.GdkWindow.Cursor = new Gdk.Cursor (Gdk.CursorType.Arrow);
                }
            }*/
            return base.OnMotionNotifyEvent (evnt);
        }
        protected override bool OnButtonPressEvent (Gdk.EventButton evnt) {
            //PointD p = new PointD (evnt.X, evnt.Y);
            switch (this.tool) {
            case SketchpadTool.Grass:
                this.scen.GreenPoint (evnt.X - Margin, evnt.Y - Margin, this.size);
                break;
            case SketchpadTool.Teleport:
                this.scen.TeleportPoint (evnt.X - Margin, evnt.Y - Margin, this.size);
                break;
            case SketchpadTool.ClearGrass:
                this.scen.ClearGreenPoint (evnt.X - Margin, evnt.Y - Margin, this.size);
                break;
            case SketchpadTool.Men:
                this.scen.setMenPoint (evnt.X - Margin, evnt.Y - Margin, this.size);
                break;
            case SketchpadTool.Wall:
            case SketchpadTool.ClearWall:
                this.oldPoint = new PointD (evnt.X - Margin, evnt.Y - Margin);
                return base.OnButtonPressEvent (evnt);
            }
            this.QueueDraw ();
            return base.OnButtonPressEvent (evnt);
        }

        protected override bool OnButtonReleaseEvent (Gdk.EventButton evnt) {
            switch (this.tool) {
            case SketchpadTool.Wall:
            case SketchpadTool.ClearWall:
                this.scen.WallPoints (this.oldPoint.X, this.oldPoint.Y, evnt.X - Margin, evnt.Y - Margin, this.size, this.tool == SketchpadTool.Wall);
                this.QueueDraw ();
                break;
            }
            return base.OnButtonReleaseEvent (evnt);
        }

        public void PaintContext (Context ctx, int w, int h) {
            this.PaintWidget (ctx, w, h);
        }

        static PointD getSize (int w, int h) {
            return new PointD (w - 2.0d * Margin, h - 2.0d * Margin);
        }

        protected override void OnSizeAllocated (Gdk.Rectangle allocation) {
            this.size = getSize (allocation.Width, allocation.Height);
            base.OnSizeAllocated (allocation);
        }

        protected override void PaintWidget (Context ctx, int w, int h) {
            base.PaintWidget (ctx, w, h);
            ctx.Save ();
            ctx.Translate (Margin, Margin);
            this.scen.Paint (ctx, this.size);
            ctx.Restore ();
        }

        public void Save (TextWriter tw) {
            this.scen.Save (tw);
        }


    }
}

