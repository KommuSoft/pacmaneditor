//
//  PacmanPiece.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2013 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using Cairo;

namespace PacmanEditor {

    public class PacmanPiece {

        private bool grass = false;
        private bool pacman = false;
        private int teleport = -0x01;
        private bool topWall = false;
        private bool leftWall = false;
        private static readonly Color greenColor = new Color (0.61960784313725, 0.67843137254902, 0.41960784313725);
        public static readonly Color GoldColor = new Color (1.0d, 0.843172549d, 0.0d);
        private static readonly float teleportStroke = 2.0f;
        private static readonly float wallStroke = 8.0f;

        public int Teleport {
            get {
                return teleport;
            }
            set {
                teleport = value;
            }
        }

        public bool LeftWall {
            get {
                return leftWall;
            }
            set {
                leftWall = value;
            }
        }

        public bool TopWall {
            get {
                return topWall;
            }
            set {
                topWall = value;
            }
        }

        public bool Pacman {
            get {
                return pacman;
            }
            set {
                pacman = value;
            }
        }

        public bool Grass {
            get {
                return grass;
            }
            set {
                grass = value;
            }
        }

        public PacmanPiece () {
        }

        public void Paint (Context ctx, PointD size) {
            if (this.grass) {
                ctx.Rectangle (0.0d, 0.0d, size.X, size.Y);
                ctx.Color = greenColor;
                ctx.Fill ();
                if (this.teleport >= 0x00) {
                    ctx.Rectangle (0.0d, 0.5d * size.Y, 0.5d * size.X, 0.5d * size.Y);
                    ctx.LineWidth = teleportStroke;
                    ctx.Color = new Color (0.0d, 0.0d, 0.0d);
                    ctx.Stroke ();
                    ctx.MoveTo (2.0d, 0.75d * size.Y);
                    ctx.ShowText ("T" + this.teleport);
                } else if (pacman) {
                    ctx.Save ();
                    ctx.Translate (0.5d * size.X, 0.5d * size.Y);
                    ctx.MoveTo (0.0d, 0.0d);
                    ctx.Arc (0.0d, 0.0d, 0.25d * size.X, 0.25d * Math.PI, 1.75d * Math.PI);
                    ctx.LineTo (0.0d, 0.0d);
                    ctx.Color = GoldColor;
                    ctx.FillPreserve ();
                    ctx.Color = new Color (0.0d, 0.0d, 0.0d);
                    ctx.Stroke ();
                    ctx.Restore ();
                } else {
                    ctx.Save ();
                    ctx.Translate (0.5d * size.X, 0.5d * size.Y);
                    ctx.Scale (0.5d, 1.0d);
                    ctx.Arc (0.0d, 0.0d, 0.25d * size.X, 0.0d, 2.0d * Math.PI);
                    ctx.Color = GoldColor;
                    ctx.FillPreserve ();
                    ctx.Color = new Color (0.0d, 0.0d, 0.0d);
                    ctx.Stroke ();
                    ctx.Restore ();
                }
                ctx.LineWidth = teleportStroke;
                ctx.Color = new Color (0.0d, 0.0d, 0.0d);
                if (topWall) {
                    ctx.MoveTo (0.0d, 0.0d);
                    ctx.LineTo (size.X, 0.0d);
                }
                if (leftWall) {
                    ctx.MoveTo (0.0d, 0.0d);
                    ctx.LineTo (0.0d, size.Y);
                }
                ctx.Stroke ();
            }
        }

        public void ClearGreen () {
            this.grass = false;
            this.teleport = -0x01;
            this.pacman = false;
            this.leftWall = false;
            this.topWall = false;
        }

        public void Green () {
            this.grass = true;
        }

        public void clearTeleport () {
            this.teleport = -0x01;
        }



    }
}

